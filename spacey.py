#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2021] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Spacey - Gradients across objects, and other stuff :)
##############################################################################


import math
import inkex
from inkex import BaseElement, Style, Group, SvgDocumentElement, transforms, colors, Transform, command
from lxml import etree
import itertools
import csv
import tempfile, os, shutil, random
from pathlib import Path
import re
import configparser


def get_attributes(self):
    for att in dir(self):
        inkex.errormsg((att, getattr(self, att)))

def read_config_file():
    csv_fun_config = configparser.ConfigParser()
    csv_fun_config.read('spacey.ini')
    if csv_fun_config.has_option('FONT', 'family'):
        font_family = csv_fun_config['FONT']['family']
        return font_family
    else:
        return None

def make_temp_svg(ink_svg_path, temp_filename):
    temp_dir = tempfile.gettempdir()
    temp_filename_path = os.path.join(temp_dir, temp_filename)
    shutil.copy2(ink_svg_path, temp_filename_path)
    return temp_filename_path

def group_wrapper(self, my_object):
    group_id = 'g' + str(random.randrange(100000, 1000000))
    new_group = self.svg.add(Group.new('#' + group_id, is_layer=True))
    new_group.append(my_object)
    new_group.attrib['id'] = group_id

    return group_id


def group_wrapper_bounding_box(self, my_object, group_id):
    if self.svg.getElementById('g' + group_id) is not None:
        new_group = self.svg.getElementById('g' + group_id)

    else:
        group_id = 'g' + group_id

        new_group = self.svg.get_current_layer().add(Group.new('#' + group_id))
        new_group.set('inkscape:groupmode', 'layer')
        new_group.attrib['id'] = group_id
    new_group.append(my_object)

    return group_id


def create_new_group(self, prefix):
    group_id = str(prefix) + '_' + str(random.randrange(100000, 999999))

    new_group = self.svg.add(Group.new('#' + group_id))

    new_group.set('inkscape:groupmode', 'layer')
    new_group.attrib['id'] = group_id

    return new_group


def query_all_bbox(self):
    my_file_path = self.options.input_file

    with tempfile.NamedTemporaryFile(mode='r+', suffix='.svg') as temp_svg_file:
        # Write the contents of the updated svg to a tempfile to use with command line
        my_svg_string = self.svg.root.tostring().decode("utf-8")
        temp_svg_file.write(my_svg_string)
        temp_svg_file.read()
        my_query = inkex.command.inkscape(temp_svg_file.name, '--query-all')
        my_query_items = my_query.split('\n')

        my_element_list = {}
        for my_query_item in my_query_items:

            my_element = my_query_item.split(',')

            # Make a dictionary for all elements, rejected malformed elements.

            if len(my_element) > 4:
                # my_element_list[my_element[0]] = my_element[1
                my_element_list[my_element[0]] = {}
                my_element_list[my_element[0]].update(x=my_element[1], y=my_element[2], width=my_element[3],
                                                      height=my_element[4])

        return my_element_list


conversions = {
    'in': 96.0,
    'pt': 1.3333333333333333,
    'px': 1.0,
    'mm': 3.779527559055118,
    'cm': 37.79527559055118,
    'm': 3779.527559055118,
    'km': 3779527.559055118,
    'Q': 0.94488188976378,
    'pc': 16.0,
    'yd': 3456.0,
    'ft': 1152.0,
    '': 1.0,  # Default px
}


def list_to_text_objects(self, ordered_objects_id, label_list):
    found_units = self.svg.unit

    unit_conversion = conversions[found_units]

    text_id_suffix = random.randrange(100000, 999999)

    # parent = self.svg.get_current_layer()

    parent = create_new_group(self, 'spacey_labels_' + str(text_id_suffix))

    text_list_with_objects = []

    ordered_objects_with_label_list = zip(ordered_objects_id, label_list)

    label_object_list = []

    font_size = self.options.font_size

    dominant_baseline = self.options.labelling_baseline_type_dropdown

    font_family = read_config_file()

    for ordered_object_id, label in ordered_objects_with_label_list:

        ordered_object = self.svg.getElementById(ordered_object_id)

        text_object = etree.SubElement(parent, inkex.addNS('text', 'svg'))

        text_object.text = str(label)

        text_object_id = 'text_label_' + str(text_id_suffix)

        text_object.attrib['id'] = text_object_id

        text_object.style['text-anchor'] = 'middle'

        text_object.style['font-family'] = str(font_family)

        text_object.style['dominant-baseline'] = dominant_baseline
        text_object.style['font-size'] = str(font_size)

        text_id_suffix += 1

        label_object_list.append(text_object)

    document_elements_query = query_all_bbox(self)

    ordered_objects_with_label_list = zip(label_object_list, ordered_objects_id)

    label_nudge_x = self.options.label_nudge_x
    label_nudge_y = self.options.label_nudge_y

    label_position = self.options.labelling_label_position_dropdown

    for label_object, ordered_object_id in ordered_objects_with_label_list:

        label_id = label_object.get_id()

        # label_half_width = (float(document_elements_query[label_id]['width']) / conversions[found_units]) / 2
        # label_half_height = (float(document_elements_query[label_id]['height']) / conversions[found_units]) / 2

        ordered_object_mid_x = (float(document_elements_query[ordered_object_id]['x']) + (float(document_elements_query[ordered_object_id]['width']) / 2)) / unit_conversion
        ordered_object_mid_y = (float(document_elements_query[ordered_object_id]['y']) + (float(document_elements_query[ordered_object_id]['height']) / 2)) / unit_conversion

        if label_position == 'centered':
            label_object.attrib['x'] = str((ordered_object_mid_x) + label_nudge_x)
            label_object.attrib['y'] = str((ordered_object_mid_y) + label_nudge_y)
        if label_position == 'top':
            label_object.attrib['x'] = str((ordered_object_mid_x) + label_nudge_x)
            label_object.attrib['y'] = str((float(document_elements_query[ordered_object_id]['y']) / unit_conversion) + label_nudge_y)
        if label_position == 'left':
            label_object.attrib['x'] = str((float(document_elements_query[ordered_object_id]['x']) / unit_conversion) - ((float(document_elements_query[label_id]['width']) / unit_conversion)) / 2  + label_nudge_x)
            label_object.attrib['y'] = str((ordered_object_mid_y) + label_nudge_y)
        if label_position == 'bottom':
            label_object.attrib['x'] = str((ordered_object_mid_x) + label_nudge_x)
            label_object.attrib['y'] = str((float(document_elements_query[ordered_object_id]['y']) / unit_conversion) + (float(document_elements_query[ordered_object_id]['height']) / unit_conversion) + (float(document_elements_query[label_id]['height']) / unit_conversion) + label_nudge_y)
        if label_position == 'right':
            label_object.attrib['x'] = str((float(document_elements_query[ordered_object_id]['x']) / unit_conversion) + (float(document_elements_query[ordered_object_id]['width']) / unit_conversion) + ((float(document_elements_query[label_id]['width']) / unit_conversion)) / 2  + label_nudge_x)
            label_object.attrib['y'] = str((ordered_object_mid_y) + label_nudge_y)


    return text_list_with_objects


# Could not find simplestyle, found this instead in extensions repo
def formatStyle(a):
    """Format an inline style attribute from a dictionary"""
    return ";".join([att + ":" + str(val) for att, val in a.items()])


def rgba_to_hex(red, green, blue, alpha):
    # All values must be padded to 2 digits

    red_hex = (format(int(red), 'x')).zfill(2)
    green_hex = (format(int(green), 'x')).zfill(2)
    blue_hex = (format(int(blue), 'x')).zfill(2)
    # Fractional value is converted to 255 first
    if alpha:
        alpha_255 = 255 * alpha
        alpha_hex = (format(int(alpha_255), 'x')).zfill(2)
    else:
        alpha_hex = ''

    return f'#{red_hex}{green_hex}{blue_hex}{alpha_hex}'


def rgb_to_hex(red, green, blue):
    # All values must be padded to 2 digits

    red_hex = (format(int(red), 'x')).zfill(2)
    green_hex = (format(int(green), 'x')).zfill(2)
    blue_hex = (format(int(blue), 'x')).zfill(2)

    return f'#{red_hex}{green_hex}{blue_hex}'


def format_bounding_box(my_object, shape_unit_fix, found_units):
    bbox = my_object.bounding_box()
    # get_attributes(bbox)
    new_bbox = {}
    if (
            my_object.TAG == 'rect' or my_object.TAG == 'ellipse' or my_object.TAG == 'circle') and shape_unit_fix == 'true':

        new_bbox["top"] = bbox.top / conversions[found_units]
        new_bbox["bottom"] = bbox.bottom / conversions[found_units]
        new_bbox["left"] = bbox.left / conversions[found_units]
        new_bbox["right"] = bbox.right / conversions[found_units]
        new_bbox["width"] = bbox.width / conversions[found_units]
        new_bbox["height"] = bbox.height / conversions[found_units]
        new_bbox["x_min"] = bbox.x.minimum / conversions[found_units]
        new_bbox["y_min"] = bbox.y.minimum / conversions[found_units]
        new_bbox["center_x"] = bbox.center[0] / conversions[found_units]
        new_bbox["center_y"] = bbox.center[1] / conversions[found_units]

        my_object.bbox_rect_x_min = new_bbox["x_min"]
        my_object.bbox_rect_y_min = new_bbox["y_min"]
        my_object.bbox_rect_width = new_bbox["width"]
        my_object.bbox_rect_height = new_bbox["height"]

    else:
        new_bbox["top"] = bbox.top
        new_bbox["bottom"] = bbox.bottom
        new_bbox["left"] = bbox.left
        new_bbox["right"] = bbox.right
        new_bbox["width"] = bbox.width
        new_bbox["height"] = bbox.height
        new_bbox["x_min"] = bbox.x.minimum
        new_bbox["y_min"] = bbox.y.minimum
        new_bbox["center_x"] = bbox.center[0]
        new_bbox["center_y"] = bbox.center[1]

        my_object.bbox_rect_x_min = new_bbox["x_min"]
        my_object.bbox_rect_y_min = new_bbox["y_min"]
        my_object.bbox_rect_width = new_bbox["width"]
        my_object.bbox_rect_height = new_bbox["height"]

    return new_bbox


def order_objects(self, my_selected, reference_point, shape_unit_fix, list_reverse):
    object_list = {}
    found_units = self.svg.unit
    bounding_box_group_id = f'bbox_{str(random.randrange(100000, 1000000))}'

    if reference_point == 'center_x':
        for my_object in my_selected:
            bbox = format_bounding_box(my_object, shape_unit_fix, found_units)
            my_object_id = my_object.get_id()
            object_list[my_object_id] = bbox["center_x"]

    elif reference_point == 'center_y':
        for my_object in my_selected:
            bbox = format_bounding_box(my_object, shape_unit_fix, found_units)
            my_object_id = my_object.get_id()
            object_list[my_object_id] = bbox["center_y"]

    elif reference_point == 'random':
        for my_object in my_selected:
            bbox = format_bounding_box(my_object, shape_unit_fix, found_units)
            bbox_point = random.randrange(0, 999)
            my_object_id = my_object.get_id()
            object_list[my_object_id] = bbox_point

    elif reference_point == 'selected':
        count = 0
        for my_object in my_selected:
            my_object_id = my_object.get_id()
            object_list[my_object_id] = count
            count += 1
            bbox = format_bounding_box(my_object, shape_unit_fix, found_units)

    elif reference_point == 'area':
        for my_object in my_selected:
            bbox = format_bounding_box(my_object, shape_unit_fix, found_units)
            bbox_area = bbox["width"] * bbox["height"]
            my_object_id = my_object.get_id()
            object_list[my_object_id] = bbox_area

    else:
        for my_object in my_selected:
            bbox = format_bounding_box(my_object, shape_unit_fix, found_units)
            my_object_id = my_object.get_id()
            bbox_point = bbox[reference_point]
            object_list[my_object_id] = bbox_point

    if list_reverse != 'true':
        ordered_objects = dict(sorted(object_list.items(), key=lambda item: item[1]))
    else:
        ordered_objects = dict(sorted(object_list.items(), key=lambda item: item[1], reverse=True))

    # Discard upper and lower values based on threshold slider.
    lower_threshold = self.options.selection_lower_threshold
    upper_threshold = self.options.selection_upper_threshold
    # inkex.errormsg(f'Lower {lower_threshold} Upper {upper_threshold}')

    # Make sure threshold does not eliminate all values

    if lower_threshold >= 0 or upper_threshold >= 0:
        ordered_objects_length = len(ordered_objects)
        lower_trim = math.ceil((ordered_objects_length / 100) * lower_threshold)
        upper_trim = ordered_objects_length - math.ceil((ordered_objects_length / 100) * upper_threshold)

        ordered_objects = dict(itertools.islice(ordered_objects.items(), lower_trim, upper_trim))

        # Fix if there are no items in ordered_objects
        if len(ordered_objects) < 1:
            ordered_objects[my_selected[0].get_id()] = 0

    if self.options.bounding_box_display == 'True':

        for my_object_id in ordered_objects:
            my_object = self.svg.getElementById(my_object_id)
            make_bounding_box(self, my_object.bbox_rect_x_min, my_object.bbox_rect_y_min, my_object.bbox_rect_width,
                              my_object.bbox_rect_height,
                              bounding_box_group_id)

    return ordered_objects


def make_bounding_box(self, x, y, width, height, bounding_box_group_id):
    # my_rect = f'<rect x={x} y={y} width={width} height={height} style="stroke:black;stroke-width:1px;">'
    # SVG element generation routine

    parent = self.svg

    style = {'stroke': self.options.color_picker_bounding_box,
             'stroke-width': '0.5',
             'fill': 'none',
             'stroke-dasharray': '1,1'
             }

    if self.options.bounding_box_pattern == 'plain':
        style.pop('stroke-dasharray', None)

    attribs = {
        'style': formatStyle(style),
        'height': str(height),
        'width': str(width),
        'x': str(x),
        'y': str(y)
    }

    my_rect = etree.SubElement(parent, inkex.addNS('rect', 'svg'), attribs)
    group_wrapper_bounding_box(self, my_rect, bounding_box_group_id)


def csv_transition(self, object_id_list):
    csv_type = ''
    transition_list = []
    my_csv_data = []

    if self.options.csv_type == 'csv_color':
        csv_filepath = self.options.csv_color_filepath
        csv_type = 'csv_color'
    if self.options.csv_type == 'csv_stroke_width':
        csv_filepath = self.options.csv_stroke_width_filepath
        csv_type = 'csv_stroke_width'
    if self.options.csv_type == 'csv_distribution':
        csv_filepath = self.options.csv_distribution_filepath
        csv_type = 'csv_distribution'

    try:
        with open(csv_filepath, newline='') as csvfile:

            my_csv_data = csv.reader(csvfile, delimiter=',', quotechar='"')

            for row in my_csv_data:
                # inkex.errormsg(row[0])
                transition_list.append(row[0])
    except:
        None

    if len(transition_list) < 1:
        return
    else:
        apply_csv_transition(self, object_id_list, transition_list, csv_type)


def apply_csv_transition(self, object_id_list, transition_list, csv_type):
    if len(transition_list) < 1:
        return

    fill_or_stroke = self.options.csv_color_fill_or_stroke

    # Repeat transition list if shorter than object list
    list_factor = math.ceil(len(object_id_list) / len(transition_list))
    # inkex.errormsg(f'LIst factor {list_factor}')
    if list_factor > 1:
        if self.options.csv_repeat == 'true':
            transition_list *= list_factor

    item_id_and_transition_list = zip(object_id_list, transition_list)

    if csv_type == 'csv_color':
        for object_id, color_value in item_id_and_transition_list:
            try:
                my_object = self.svg.getElementById(object_id)
                my_object.style.set_color(color_value, name=fill_or_stroke)

            except:
                None

    if csv_type == 'csv_stroke_width':
        # inkex.errormsg(len(transition_list))
        for object_id, stroke_width_value in item_id_and_transition_list:
            try:
                my_object = self.svg.getElementById(object_id)
                my_object.style['stroke-width'] = stroke_width_value

            except:
                None

    if csv_type == 'csv_distribution':

        found_units = self.svg.unit
        # csv_shape_unit_fix = self.options.csv_shape_unit_fix

        csv_shape_unit_fix = self.options.shape_unit_fix

        # For fraction option ( objects are space within original centre to centre bounds )
        cumulative_total = 0
        # Total the distribution values for number of objects
        transition_list_ints = [int(item) for item in transition_list]
        dist_list = transition_list_ints[0:len(object_id_list)]
        dist_list_total = sum(dist_list)

        # Get Centre for first and last items
        first_object = self.svg.selected[0]
        last_object = self.svg.selected[-1]

        # objects_center_to_center_x = last_object_center[0] - first_object_center[0]

        cumulative_shift = 0

        if (
                first_object.TAG == 'rect' or first_object.TAG == 'ellipse' or first_object.TAG == 'circle') and csv_shape_unit_fix == 'true':
            first_object_center = first_object.bounding_box().center.x / conversions[found_units]
            last_object_center = last_object.bounding_box().center.x / conversions[found_units]

        else:
            first_object_center = first_object.bounding_box().center.x
            last_object_center = last_object.bounding_box().center.x

        center_to_center = last_object_center - first_object_center

        fractional_unit_cumulative = center_to_center / dist_list_total
        fractional_unit = center_to_center / dist_list[-1]

        for object_id, distribution_value in item_id_and_transition_list:
            distribution_value = float(distribution_value)
            my_object = self.svg.getElementById(object_id)

            cumulative_shift += distribution_value

            if (
                    my_object.TAG == 'rect' or my_object.TAG == 'ellipse' or my_object.TAG == 'circle') and csv_shape_unit_fix == 'true':

                x_shift_to_first = first_object_center - (my_object.bounding_box().center.x / conversions[found_units])

            else:

                x_shift_to_first = first_object_center - my_object.bounding_box().center.x

            # Return all objects to coords of 1st object centre

            my_object.transform.add_translate(x_shift_to_first)

            # Choose Absolute or Relative
            if self.options.csv_distribution_fraction == 'absolute':
                object_shift = distribution_value

            if self.options.csv_distribution_fraction == 'fractional':
                object_shift = fractional_unit * distribution_value

            if self.options.csv_distribution_fraction == 'cumulative':
                object_shift = fractional_unit_cumulative * cumulative_shift

            my_object.transform.add_translate(object_shift)


def nth_child_transition(self, object_id_list):
    n_value = self.options.n_value
    nth_child_offset_value = self.options.nth_child_offset_value

    # Convert dictionary keys to a list
    key_list = list(object_id_list.keys())

    nth_child_list = key_list[nth_child_offset_value::n_value]

    picked_color = self.options.color_picker_nth_child_value.to_rgba()

    picked_color_red = picked_color[0]
    picked_color_green = picked_color[1]
    picked_color_blue = picked_color[2]
    picked_color_alpha = picked_color[3]

    my_hex_color = rgb_to_hex(picked_color_red, picked_color_green, picked_color_blue)

    if self.options.fill_or_stroke_nth_child_transition == 'fill':

        for item in nth_child_list:
            my_object = self.svg.getElementById(item)
            my_object.style.set_color(my_hex_color, name="fill")
            my_object.style["fill-opacity"] = picked_color_alpha

    else:
        for item in nth_child_list:
            my_object = self.svg.getElementById(item)
            my_object.style.set_color(my_hex_color, name="stroke")
            my_object.style["stroke-opacity"] = picked_color_alpha

    # inkex.errormsg(nth_child_list)


def two_color_fractional_gradient(self, object_id_list):
    red_formula = self.options.red_transition_type
    green_formula = self.options.green_transition_type

    blue_formula = self.options.blue_transition_type
    alpha_formula = self.options.alpha_transition_type

    red_power = self.options.red_transition_power
    green_power = self.options.green_transition_power

    blue_power = self.options.blue_transition_power
    alpha_power = self.options.alpha_transition_power

    object_list_length = len(object_id_list)

    color_one = self.options.color_picker_one_value.to_rgba()
    color_two = self.options.color_picker_two_value.to_rgba()

    color_one_red = color_one[0]
    color_one_green = color_one[1]

    color_one_blue = color_one[2]
    color_one_alpha = color_one[3]

    color_two_red = color_two[0]
    color_two_green = color_two[1]
    color_two_blue = color_two[2]
    color_two_alpha = color_two[3]

    if self.options.red_transition_checkbox == 'true':
        red_fraction_list = make_effect_fraction_list(self, object_id_list, red_formula, red_power)
        red_range = (color_two_red - color_one_red)
        red_list = [int(i * red_range) for i in red_fraction_list]
    else:
        red_list = [0] * len(object_id_list)
    if self.options.green_transition_checkbox == 'true':
        green_fraction_list = make_effect_fraction_list(self, object_id_list, green_formula, green_power)
        green_range = (color_two_green - color_one_green)
        green_list = [int(i * green_range) for i in green_fraction_list]

    else:
        green_list = [0] * len(object_id_list)

    if self.options.blue_transition_checkbox == 'true':
        blue_fraction_list = make_effect_fraction_list(self, object_id_list, blue_formula, blue_power)
        blue_range = (color_two_blue - color_one_blue)
        blue_list = [int(i * blue_range) for i in blue_fraction_list]

    else:
        blue_list = [0] * len(object_id_list)

    if self.options.alpha_transition_checkbox == 'true':
        alpha_fraction_list = make_effect_fraction_list(self, object_id_list, alpha_formula, alpha_power)
        alpha_range = (color_two_alpha - color_one_alpha)
        alpha_list = [(i * alpha_range) for i in alpha_fraction_list]
    else:
        alpha_list = [0] * len(object_id_list)

    item_id_and_color_list = zip(object_id_list, red_list, green_list, blue_list, alpha_list)

    for item, red_step, green_step, blue_step, alpha_step in item_id_and_color_list:

        my_object = self.svg.getElementById(item)

        my_red_value = color_one_red + red_step
        my_green_value = color_one_green + green_step
        my_blue_value = color_one_blue + blue_step
        my_alpha_value = color_one_alpha + alpha_step

        my_hex_color = rgb_to_hex(my_red_value, my_green_value, my_blue_value)

        if self.options.fill_or_stroke_color_transition == 'fill':
            my_object.style.set_color(my_hex_color, name="fill")
            my_object.style["fill-opacity"] = my_alpha_value
        else:
            my_object.style.set_color(my_hex_color, name="stroke")
            my_object.style["stroke-opacity"] = my_alpha_value


def make_effect_fraction_list(self, my_selected, formula, power):
    list_length = len(my_selected)
    processed_list = []

    if formula == 'x^y':
        for item in range(list_length):
            if item > 0:
                step_size = math.pi / list_length
                list_item = item ** power
                processed_list.append(list_item)
            else:
                list_item = item
                processed_list.append(list_item)

            list_max = max(processed_list)
            if list_max > 0:
                fraction_list = [(list_item / list_max) for list_item in processed_list]
            else:
                fraction_list = [list_item for list_item in processed_list]

    if formula == 'fraction':
        fraction_value = 1
        count = 1
        for item in range(list_length):
            if item > 0:
                step_size = math.pi / list_length
                list_item = fraction_value
                processed_list.append(list_item)
                fraction_value = fraction_value + fraction_value / count ** 2
                count += 1
            else:
                list_item = item
                processed_list.append(list_item)
            # inkex.errormsg(processed_list)
            list_max = max(processed_list)
            if list_max > 0:
                fraction_list = [(list_item / list_max) for list_item in processed_list]
            else:
                fraction_list = [list_item for list_item in processed_list]

    if formula == 'sin':

        # First make a list using forumla and list items
        # for item in range(1, list_length):
        for item in range(list_length):
            if item > 0:
                step_size = math.pi / list_length
                list_item = math.sin(item * step_size)
                processed_list.append(list_item)
            else:
                list_item = item
                processed_list.append(list_item)

            list_max = max(processed_list)
            if list_max > 0:
                fraction_list = [(list_item / list_max) for list_item in processed_list]
            else:
                fraction_list = [list_item for list_item in processed_list]

    elif formula == 'x':
        count = 0
        for item in range(list_length):
            list_item = count
            count += 1
            processed_list.append(list_item)

        list_max = max(processed_list)
        if list_max > 0:
            fraction_list = [(list_item / list_max) for list_item in processed_list]
        else:
            fraction_list = [list_item for list_item in processed_list]

    return fraction_list


def make_labels(self, ordered_objects):
    labelling_type = self.options.labelling_type

    labelling_string = self.options.labelling_string

    labelling_case = self.options.labelling_case

    if labelling_type == 'letters' and labelling_case == 'labelling_uppercase':
        char_set = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
                    'S',
                    'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    elif labelling_type == 'letters' and labelling_case == 'labelling_lowercase':
        char_set = 'abcdefghijklmnopqrstuvwxyz'
    elif labelling_type == 'greek':
        char_set = ['α', 'β', 'γ', 'δ', 'ε', 'ζ', 'η', 'θ', 'ι', 'κ', 'λ', 'μ', 'ν', 'ξ', 'o', 'π', 'ρ', 'σ',
                    'τ',
                    'υ', 'φ', 'χ', 'ψ', 'ω']
    elif labelling_type == 'russian' and labelling_case == 'labelling_lowercase':
        char_set = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
    elif labelling_type == 'russian' and labelling_case == 'labelling_uppercase':
        char_set = 'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ'

    elif labelling_type == 'numbers':
        char_set = range(1, 99999, 1)

    elif labelling_type == 'custom':
        pattern = re.compile(r'(,){2,}')
        labelling_string = re.sub(pattern, ',', labelling_string)
        char_set = labelling_string.split(',')
        char_set = list(map(str.strip, char_set))

    # query_all_bbox(self)
    my_objects = self.svg.selected

    no_of_objects = len(ordered_objects)
    char_set_length = len(char_set)
    object_count = 1
    label_list = []
    for my_object in ordered_objects:

        value = object_count // char_set_length
        count = math.ceil(object_count / char_set_length)

        if value > 0 and count > 1:  # count > 1 ignores last letter of first run
            label = char_set[(object_count % char_set_length) - 1] + str(count)
        else:
            label = char_set[object_count - 1]
        object_count += 1
        label_list.append(label)
    # inkex.errormsg(label_list)

    list_to_text_objects(self, ordered_objects, label_list)


class Spacey(inkex.EffectExtension):

    def add_arguments(self, pars):
        pars.add_argument("--shapeUnitFix", type=str, dest="shape_unit_fix")
        pars.add_argument("--reverseObjectOrder", type=str, dest="reverse_object_order")
        pars.add_argument("--selection_order_by_dropdown", type=str, dest="selection_order_by", default='selected')
        pars.add_argument("--selection_lower_threshold", type=int, dest="selection_lower_threshold", default=0)
        pars.add_argument("--selection_upper_threshold", type=int, dest="selection_upper_threshold", default=0)

        pars.add_argument("--color_picker_one", type=inkex.colors.Color, dest="color_picker_one_value", default=0)
        pars.add_argument("--color_picker_two", type=inkex.colors.Color, dest="color_picker_two_value", default=0)

        pars.add_argument("--spacey", type=str, dest="spacey_notebook_page", default=0)

        pars.add_argument("--bounding_box_display", type=str, dest="bounding_box_display", default='none')
        pars.add_argument("--bounding_box_pattern", type=str, dest="bounding_box_pattern", default='plain')

        pars.add_argument("--color_picker_bounding_box", type=inkex.colors.Color, dest="color_picker_bounding_box",
                          default=0)

        pars.add_argument("--effect_type_dropdown", type=str, dest="effect_type", default='effect_none')

        # Transition page

        # STROKE OR FILL COLOUR
        pars.add_argument("--fill_or_stroke_color_transition", type=str, dest="fill_or_stroke_color_transition",
                          default='fill')

        # RED
        pars.add_argument("--red_transition_checkbox", type=str, dest="red_transition_checkbox")
        pars.add_argument("--red_transition_dropdown", type=str, dest="red_transition_type", default='x')
        pars.add_argument("--red_transition_power", type=float, dest="red_transition_power", default=1)

        # GREEN
        pars.add_argument("--green_transition_checkbox", type=str, dest="green_transition_checkbox")
        pars.add_argument("--green_transition_dropdown", type=str, dest="green_transition_type", default='x')
        pars.add_argument("--green_transition_power", type=float, dest="green_transition_power", default=1)

        # BLUE
        pars.add_argument("--blue_transition_checkbox", type=str, dest="blue_transition_checkbox")
        pars.add_argument("--blue_transition_dropdown", type=str, dest="blue_transition_type", default='x')
        pars.add_argument("--blue_transition_power", type=float, dest="blue_transition_power", default=1)

        # ALPHA
        pars.add_argument("--alpha_transition_checkbox", type=str, dest="alpha_transition_checkbox")
        pars.add_argument("--alpha_transition_dropdown", type=str, dest="alpha_transition_type", default='x')
        pars.add_argument("--alpha_transition_power", type=float, dest="alpha_transition_power", default=1)

        # STROKE OR FILL NTH CHILD
        pars.add_argument("--fill_or_stroke_nth_child_transition", type=str, dest="fill_or_stroke_nth_child_transition",
                          default='fill')
        pars.add_argument("--color_picker_nth_child", type=inkex.colors.Color, dest="color_picker_nth_child_value",
                          default=0)
        pars.add_argument("--n_value", type=int, dest="n_value", default=1)
        # pars.add_argument("--nth_child_offset_checkbox", type=str, dest="nth_child_offset_checkbox", default='false')
        pars.add_argument("--nth_child_offset_value", type=int, dest="nth_child_offset_value", default=1)

        # CSV

        pars.add_argument("--csv_type_dropdown", type=str, dest="csv_type", default=None)

        pars.add_argument("--csv_repeat", type=str, dest="csv_repeat", default='true')

        pars.add_argument("--csv_color_filepath", type=str, dest="csv_color_filepath", default=None)
        pars.add_argument("--csv_color_fill_or_stroke", type=str, dest="csv_color_fill_or_stroke", default='fill')

        pars.add_argument("--csv_stroke_width_filepath", type=str, dest="csv_stroke_width_filepath", default=None)

        pars.add_argument("--csv_distribution_filepath", type=str, dest="csv_distribution_filepath", default=None)
        pars.add_argument("--csv_distribution_fraction", type=str, dest="csv_distribution_fraction", default='absolute')

        # pars.add_argument("--csv_shape_unit_fix", type=str, dest="csv_shape_unit_fix")

        # LABELLING

        pars.add_argument("--labelling_type_dropdown", type=str, dest="labelling_type", default='x')

        pars.add_argument("--labelling_string", type=str, dest="labelling_string", default='x')

        pars.add_argument("--labelling_case", type=str, dest="labelling_case", default='labelling_lowercase')

        pars.add_argument("--label_nudge_x", type=float, dest="label_nudge_x", default=0)

        pars.add_argument("--label_nudge_y", type=float, dest="label_nudge_y", default=0)

        pars.add_argument("--font_size", type=float, dest="font_size", default=12)

        pars.add_argument("--labelling_label_position_dropdown", type=str, dest="labelling_label_position_dropdown", default='auto')

        pars.add_argument("--labelling_baseline_type_dropdown", type=str, dest="labelling_baseline_type_dropdown", default='auto')

    def effect(self):
        found_units = self.svg.unit

        my_selected = self.svg.selected

        if len(my_selected) < 1:
            return

        ordered_objects = order_objects(self, my_selected, self.options.selection_order_by,
                                        self.options.shape_unit_fix,
                                        self.options.reverse_object_order)

        if self.options.effect_type == "effect_color_transition":
            two_color_fractional_gradient(self, ordered_objects)
        if self.options.effect_type == "effect_nth_child":
            nth_child_transition(self, ordered_objects)
        if self.options.effect_type == "effect_csv":
            csv_transition(self, ordered_objects)
        if self.options.effect_type == "effect_labelling":
            make_labels(self, ordered_objects)



if __name__ == '__main__':
    Spacey().run()
