#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2021] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# csv fun font - sets the font for the csv_fun csv importer into Inkscape
##############################################################################

import inkex
import configparser
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from gi.repository import Gtk, GdkPixbuf, Gdk
import configparser


def get_attributes(self):
    for att in dir(self):
        inkex.errormsg((att, getattr(self, att)))


def write_config_file(font_family):
    spacey_config = configparser.ConfigParser()
    spacey_config['FONT'] = {}
    spacey_config['FONT']['family'] = font_family

    with open('spacey.ini', 'w') as configfile:
        spacey_config.write(configfile)


class Handler:

    def onDestroy(self, *args):
        Gtk.main_quit()
        font_family = SpaceyFont.builder.get_object('gtk_font_chooser').get_font_desc().get_family()
        write_config_file(font_family)

    def gtkFontChooser(self, scale):
        font_family = SpaceyFont.builder.get_object('gtk_font_chooser').get_font_desc().get_family()


def run_gtk():
    # Build interface from .glade file
    SpaceyFont.builder = Gtk.Builder()
    SpaceyFont.builder.add_from_file("spacey_font.glade")
    SpaceyFont.builder.connect_signals(Handler())

    SpaceyFont.window = SpaceyFont.builder.get_object("main_window")
    SpaceyFont.window.show_all()
    SpaceyFont.window.set_title('Spacey Font')

    # End of Gtk main loop
    Gtk.main()


class SpaceyFont(inkex.EffectExtension):

    def effect(self):
        run_gtk()


if __name__ == '__main__':
    SpaceyFont().run()
