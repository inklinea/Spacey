Spacey - An Inkscape Extension

Inkscape 1.1+

** Appears In Extensions Menu Under 'Arrange' **

Just drop the spacey folder into the Inkscape extensions folder. 

( It is not necessary to put individual files in the extension folder root)

▶ Gradients across objects

▶ nth Child Colour

▶ csv Colour list import ( uses only the first column which can be css colour name or #hex value, or rgb(1,2,3) value )
      (some samples are included)

▶ csv Horiztontal distribution of object

▶ Labelling of objects ( Numbers, Letters, Custom List)
	
▶ Can crash to the desktop during live preview sometimes - please be aware

▶ Works on simple objects - rectangle, circle, ellipse, path etc

▶ Does not work on clones, or groups.

▶ For repeated csv distribution, transforms must be applied see:
https://github.com/Klowner/inkscape-applytransforms

